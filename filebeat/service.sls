# stop and disable default service
# we will create and run new instance for each Graylog's input

filebeat.service-default:
  service.dead:
    - name: filebeat
    - enable: False

{% for app in salt['pillar.get']('filebeat')  %}
filebeat.unit-{{ app }}:
  file.managed:
    - name: /etc/systemd/system/filebeat-{{ app }}.service
    - user: root
    - group: root
    - mode: 644
    - contents: |
        [Unit]
        Description=filebeat-{{ app }}
        Documentation=https://www.elastic.co/guide/en/beats/filebeat/current/index.html
        Wants=network-online.target
        After=network-online.target

        [Service]
        ExecStart=/usr/share/filebeat/bin/filebeat -c /etc/filebeat/filebeat-{{ app }}.yml

        Restart=always

        [Install]
        WantedBy=multi-user.target
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: filebeat.unit-{{ app }}

filebeat.service-{{ app }}:
  service.running:
    - name: filebeat-{{ app }}
    - enable: True
    - reload: True
    - require:
      - pkg: filebeat
    - watch:
      - file: filebeat.config-{{ app }}

{% endfor %}
