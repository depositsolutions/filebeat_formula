# filebeat formula
Install and configure filebeat.

See the full [Salt Formulas installation and usage instructions](http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html).

## Available states

* filebeat.install
* filebeat.config
* filebeat.service

## filebeat.install

Installs the filebeat package.

## filebeat.config

Creates configuration files to log different applications.

### Usage

See pillar.example for example configuration.

## filebeat.service

Creates systemd unit files and starts filebeat services.

